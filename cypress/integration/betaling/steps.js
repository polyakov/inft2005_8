import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity")
    .clear()
    .type("4");
  cy.get("#saveItem").click();
  cy.get("#product").select("Smørbukk");
  cy.get("#quantity")
    .clear()
    .type("5");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get("#fullName")
      .clear()
      .type("Knut Knutsen")
      .blur();

    cy.get("#address")
      .clear()
      .type("Moholt Alle 3-22")
      .blur();

    cy.get("#postCode")
      .clear()
      .type("7040")
      .blur();

    cy.get("#city")
      .clear()
      .type("Trondheim")
      .blur();

    cy.get("#creditCardNo")
      .clear()
      .type("1234123412341234")
      .blur();
  }
);

And(/^trykker på Fullfør kjøp$/, () => {
  cy.get("input[type=submit]").click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, function() {
  cy.get(".confirmation")
    .first()
    .should("contain", "Din ordre er registrert.");
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity")
    .clear()
    .type("4");
  cy.get("#saveItem").click();
  cy.get("#product").select("Smørbukk");
  cy.get("#quantity")
    .clear()
    .type("5");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get("#fullName")
    .clear()
    .blur();
  cy.get("#address")
    .clear()
    .blur();
  cy.get("#postCode")
    .clear()
    .blur();
  cy.get("#city")
    .clear()
    .blur();
  cy.get("#creditCardNo")
    .clear()
    .blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, function() {
  cy.get("#fullNameError").should("not.be.empty");
  cy.get("#addressError").should("not.be.empty");
  cy.get("#postCodeError>.formError").should("not.be.empty");
  cy.get("#cityError").should("not.be.empty");
  cy.get("#creditCardNoError").should("not.be.empty");
});
